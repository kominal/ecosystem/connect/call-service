# Call Service

The call service is used to transfer voice and video data between clients. Every traffic on this service is fully encrypted using AES on the client side.

## Documentation

Production: https://connect.kominal.com/call-service/api-docs
Test: https://connect-test.kominal.com/call-service/api-docs
