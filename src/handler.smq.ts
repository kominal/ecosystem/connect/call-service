import { Packet } from '@kominal/service-util/model/packet';
import service from '.';
import { info, error, debug } from '@kominal/service-util/helper/log';
import SocketConnectionDatabase from '@kominal/connect-models/socketconnection/socketconnection.call.database';

export class SMQHandler {
	async onConnect(): Promise<void> {
		info('Connected to SwarmMQ cluster.');
		const smqClient = service.getSMQClient();
		if (smqClient) {
			smqClient.subscribe('QUEUE', 'CALL.SYNC');
			smqClient.subscribe('QUEUE', 'CALL.CONNECT');
			smqClient.subscribe('QUEUE', 'CALL.DISCONNECT');
			smqClient.subscribe('QUEUE', 'CALL.REFRESH');
			smqClient.subscribe('QUEUE', 'INSECURE.CALL');
		}
	}

	async onDisconnect(): Promise<void> {
		error('Lost connection to SwarmMQ cluster.');
	}

	async onMessage(packet: Packet): Promise<void> {
		const { destinationType, destinationName, event } = packet;
		if (!event) {
			return;
		}

		const { type, body } = event;
		if (destinationType === 'QUEUE' && destinationName === 'CALL.SYNC') {
			this.onSync(type, body);
		} else if (destinationType === 'QUEUE' && destinationName === 'CALL.CONNECT') {
			this.onUserConnect(body);
		} else if (destinationType === 'QUEUE' && destinationName === 'CALL.DISCONNECT') {
			this.onUserDisconnect(body);
		} else if (destinationType === 'QUEUE' && destinationName === 'CALL.REFRESH') {
			this.onUserRefresh(body);
		} else if (destinationType === 'QUEUE' && destinationName === 'INSECURE.CALL') {
			this.onEvent(event);
		}
	}

	async onSync(type: string, body: any) {
		if (type === 'SYNC') {
			const { taskIds } = body;
			info(`Starting sync for the following socket services: ${taskIds}`);
			const socketConnections = await SocketConnectionDatabase.find({ taskId: { $not: { $in: taskIds } } });
			for (const socketConnection of socketConnections) {
				this.onUserDisconnect({
					taskId: socketConnection.get('taskId'),
					connectionId: socketConnection.get('connectionId'),
					userId: socketConnection.get('userId'),
				});
			}
			const result = await SocketConnectionDatabase.deleteMany({ taskId: { $not: { $in: taskIds } } });
			if (result.deletedCount && result.deletedCount > 0) {
				info(`Removed ${result.deletedCount} dead socket connections!`);
			}
		} else if (type === 'RESET') {
			const taskId: string = body.taskId;
			info(`Starting reset for the following socket service: ${taskId}`);
			const connections: { connectionId: string; userId: string }[] = body.connections;
			const connectionIds = connections.map((c) => c.connectionId);
			const socketConnections = await SocketConnectionDatabase.find({ taskId, connectionId: { $not: { $in: connectionIds } } });
			for (const socketConnection of socketConnections) {
				this.onUserDisconnect({ taskId, connectionId: socketConnection.get('connectionId'), userId: socketConnection.get('userId') });
			}

			for (const connection of connections) {
				const { connectionId, userId } = connection;
				if ((await SocketConnectionDatabase.countDocuments({ taskId, connectionId, userId })) === 0) {
					this.onUserConnect({ taskId, connectionId, userId, interestingGroupIds: [] });
					service.getSMQClient()?.publish('TOPIC', `INTERNAL.REFRESH.${userId}`, 'REFRESH');
				}
			}
		}
	}

	async onUserConnect(body: { taskId: string; connectionId: string; userId: string; interestingGroupIds: string[] }) {
		const { taskId, connectionId, userId, interestingGroupIds } = body;

		await SocketConnectionDatabase.findOneAndUpdate({ connectionId, taskId }, { userId, interestingGroupIds }, { upsert: true });

		const smqClient = service.getSMQClient();
		if (smqClient) {
			for (const groupId of interestingGroupIds) {
				const members: any[] = [];
				for (const socketConnection of await SocketConnectionDatabase.find({ groupId })) {
					members.push({
						taskId: socketConnection.get('taskId'),
						connectionId: socketConnection.get('connectionId'),
						userId: socketConnection.get('userId'),
					});
				}
				if (members.length > 0) {
					smqClient.publish('TOPIC', `DIRECT.CONNECTION.${connectionId}`, 'CALL.MEMBERS', { groupId, members });
				}
			}
		}

		debug(`Connection from user ${userId}, connection ${connectionId} established. ${interestingGroupIds}`);
	}

	async onUserRefresh(body: { userId: string; interestingGroupIds: string[] }) {
		const smqClient = service.getSMQClient();
		if (smqClient) {
			const { userId, interestingGroupIds } = body;
			for (const socketConnection of await SocketConnectionDatabase.find({ userId })) {
				const taskId = socketConnection.get('taskId');
				const connectionId = socketConnection.get('connectionId');

				const previousInterestingGroupIds: string[] = socketConnection.get('interestingGroupIds');
				const newInterestingGroupIds = interestingGroupIds.filter((g) => !previousInterestingGroupIds.includes(g));
				const oldInterestingGroupIds = previousInterestingGroupIds.filter((g) => !interestingGroupIds.includes(g));

				const groupId = socketConnection.get('groupId');
				if (groupId && oldInterestingGroupIds.includes(groupId)) {
					await this.onCallLeave({ taskId, connectionId, userId });
				}

				for (const groupId of oldInterestingGroupIds) {
					smqClient.publish('TOPIC', `DIRECT.CONNECTION.${connectionId}`, 'CALL.MEMBERS', { groupId, members: [] });
				}

				for (const groupId of newInterestingGroupIds) {
					const members: any[] = [];
					for (const socketConnection of await SocketConnectionDatabase.find({ groupId })) {
						members.push({
							taskId: socketConnection.get('taskId'),
							connectionId: socketConnection.get('connectionId'),
							userId: socketConnection.get('userId'),
						});
					}
					if (members.length > 0) {
						smqClient.publish('TOPIC', `DIRECT.CONNECTION.${connectionId}`, 'CALL.MEMBERS', { groupId, members });
					}
				}

				await SocketConnectionDatabase.findOneAndUpdate({ taskId, connectionId }, { interestingGroupIds });
			}
		}
	}

	async onUserDisconnect(body: { taskId: string; connectionId: string; userId: string }) {
		const { taskId, connectionId, userId } = body;

		await this.onCallLeave(body);

		await SocketConnectionDatabase.deleteMany({ taskId, connectionId });

		debug(`Connection from user ${userId}, connection ${connectionId} lost.`);
	}

	async onEvent(event: { type: string; body?: any }): Promise<void> {
		const { type, body } = event;
		if (type === 'CALL.JOIN') {
			this.onCallJoin(body);
		} else if (type === 'CALL.LEAVE') {
			this.onCallLeave(body);
		} else if (type === 'CALL.SIGNALING') {
			this.onCallSignaling(body);
		}
	}

	async onCallJoin(body: { taskId: string; connectionId: string; userId: string; groupId: string }) {
		const { taskId, connectionId, userId, groupId } = body;

		await this.onCallLeave(body);

		const smqClient = service.getSMQClient();
		const socketConnection = await SocketConnectionDatabase.findOne({ taskId, connectionId });
		if (smqClient && socketConnection) {
			const interestingGroupIds: string[] = socketConnection.get('interestingGroupIds');
			if (interestingGroupIds.includes(groupId)) {
				const existingCall = await SocketConnectionDatabase.countDocuments({ groupId });
				await SocketConnectionDatabase.findOneAndUpdate({ connectionId, taskId }, { groupId }, { upsert: true });
				if (existingCall) {
					smqClient.publish('TOPIC', `DIRECT.GROUP.${groupId}`, 'CALL.JOIN', { groupId, userId, connectionId });
				} else {
					smqClient.publish('TOPIC', `DIRECT.GROUP.${groupId}`, 'CALL.START', { groupId, userId, connectionId });
				}
			}
		}

		console.log(`User ${userId} wants to join call ${groupId} using connection ${connectionId}.`);
	}

	async onCallSignaling(body: {
		taskId: string;
		connectionId: string;
		userId: string;
		groupId: string;
		targetConnectionId: string;
		signaling: string;
	}) {
		const { taskId, connectionId, userId, targetConnectionId, signaling } = body;

		console.log(`Connection ${connectionId} wants to send a packet to ${targetConnectionId}.`);

		const socketConnection = await SocketConnectionDatabase.findOne({ connectionId });
		if (socketConnection) {
			const groupId = socketConnection.get('groupId');
			const targetSocketConnection = await SocketConnectionDatabase.findOne({ groupId, connectionId: targetConnectionId });
			if (targetSocketConnection) {
				service
					.getSMQClient()
					?.publish('TOPIC', `DIRECT.CONNECTION.${targetConnectionId}`, 'CALL.SIGNALING', { userId, connectionId, groupId, signaling });
			}
		}
	}

	async onCallLeave(body: { taskId: string; connectionId: string; userId: string }) {
		const { taskId, connectionId, userId } = body;

		console.log(`User ${userId} wants to leave the current call using connection ${connectionId}.`);

		const socketConnection = await SocketConnectionDatabase.findOne({ taskId, connectionId });
		if (socketConnection) {
			const groupId: string = socketConnection.get('groupId');
			if (groupId) {
				await socketConnection.updateOne({ groupId: undefined });
				const smqClient = service.getSMQClient();
				if (smqClient) {
					smqClient.publish('TOPIC', `DIRECT.GROUP.${groupId}`, 'CALL.LEAVE', { groupId, userId, connectionId });
				}
			}
		}
	}
}
