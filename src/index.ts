import Service from '@kominal/service-util/helper/service';
import relay from './routes/relay';
import check from './check';
import { SMQHandler } from './handler.smq';

const service = new Service({
	id: 'call-service',
	name: 'Call Service',
	description: 'Manages calls and relays encrypted media traffic.',
	jsonLimit: '16mb',
	routes: [relay],
	database: true,
	squad: false,
	scheduler: [{ task: check, squad: false, frequency: 30 }],
	swarmMQ: new SMQHandler(),
});
service.start();

export default service;
