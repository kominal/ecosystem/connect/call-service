import { info, error } from '@kominal/service-util/helper/log';
const isPortReachable = require('is-port-reachable');
import Docker from 'dockerode';
export let entrypoints: string[] = [];

export default async function check() {
	info('Checking health of relay services...');

	const nodes = await new Docker().listNodes();
	const relayServices = nodes
		.filter((node) => node.Spec.Labels.relay === 'true')
		.map((node) => {
			return {
				addr: node.Status.Addr,
				hostname: node.Description.Hostname,
			};
		});

	const newEntrypoints: string[] = [];
	for (const relayService of relayServices) {
		try {
			const reachable = await isPortReachable(3478, { host: relayService.addr });
			if (reachable) {
				newEntrypoints.push(relayService.addr);
			} else {
				error(`Relay service on '${relayService.hostname}' is unreachable.`);
			}
		} catch (err) {
			error(`Could not resolve ip for '${relayService.hostname}'.`);
		}
	}
	entrypoints = newEntrypoints;
}
